"""
	bool = Base.:(==)(a::T,b::T) where T <: AbstractConvexElement

Implements equality comparison for convex elements. They are the same if they
reference the same node indices in the same order.
"""
function Base.:(==)(a::T,b::T) where T <: AbstractConvexElement

	a.nodeI == b.nodeI

end # function

"""
	bool = Base.:(==)(a::T,b::T) where T <: AbstractSimplexElement

Implements equality comparison for simplical elements. They are the same if
they contain the same nodes.
"""
function Base.:(==)(a::T,b::T) where T <: AbstractSimplexElement

	sort(a.nodeI) == sort(b.nodeI)

end # function
