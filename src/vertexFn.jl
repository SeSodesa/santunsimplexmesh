"""
	vertices = vertexFn(element::AbstractConvexElement, nodes::AbstractArray{Node{N}}) where N

Extracts the verticex points related to an element.
"""
function vertexFn(element::AbstractConvexElement, nodes::AbstractArray{Node{N}}) where N

	nodes[element]

end # function

"""
	vertices = vertexFn(elements::AbstractArray{<:AbstractConvexElement}, nodes::AbstractArray{Node{N}}) where N

Extracts the verticex points related to an element.
"""
function vertexFn(elements::AbstractArray{<:AbstractConvexElement}, nodes::AbstractArray{Node{N}}) where N

	reduce( vcat, [ vertexFn( element, nodes ) for element in elements ] )

end # function
