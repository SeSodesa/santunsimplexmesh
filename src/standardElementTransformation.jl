"""
	error = standardElementTransformation(element::AbstractSimplexElement, nodes::AbstractArray{Node{N}}) where N

This transformation is not defined for an arbitrary abstract simplical element.
You will need to define it for each concrete combination of simplical element
and a set of nodes of dimension N.
"""
function standardElementTransformation(element::AbstractSimplexElement, nodes::AbstractArray{Node{N}}) where N

	error("A general standard element transformation is not defined for an absract simplical element. Please define standardElementTransformation for $(typeof(element)).")

end # function

"""
	T, r = standardElementTransformation(element::Triangle, nodes::AbstractArray{Node{2}})

Given an 2-dimensional triangle, produces a transformation (T ⋅  + r), mapping
the vertices r1, r2 and r3 of a standard planar triangle x, y ≥ 0, x + y ≤ 1 to
the skewed and translated element.
"""
function standardElementTransformation(element::Triangle, nodes::AbstractArray{Node{2}})

	elementVertices = vertexFn(element,nodes)

	firstVertex = elementVertices[begin]

	otherVertices = elementVertices[2:end]

	planeVecs = [ otherVertex - firstVertex for otherVertex in otherVertices ]

	T, r = hcat(planeVecs...), firstVertex

end # function

"""
	T, r = standardElementTransformation(element::Tetrahedron, nodes::AbstractArray{Node{3}})

Given a tetrahedron, produces an affine transformation (T ⋅ + r), mapping the
vertices r1, r2, r3 and r4 of a standard tetrahedron x, y, z ≥ 0, x + y + z ≤ 1
to the skewed and translated element.
"""
function standardElementTransformation(element::Tetrahedron, nodes::AbstractArray{Node{3}})

	elementVertices = vertexFn(element,nodes)

	firstVertex = elementVertices[begin]

	otherVertices = elementVertices[2:end]

	planeVecs = [ otherVertex - firstVertex for otherVertex in otherVertices ]

	T, r = hcat(planeVecs...), firstVertex

end # function
