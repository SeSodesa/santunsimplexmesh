"""
	TetraFaceNeighbourRelation <: AbstractNeighbourRelation

Represents an face-based adjacency relation between tetrahedra.
"""
struct TetraFaceNeighbourRelation <: AbstractNeighbourRelation

	relation :: Vector{SVector{4,Union{Missing,Int}}}

end # struct

"""
	TetraFaceNeighbourRelation(elements::AbstractArray{Tetrahedron})

An external constructor for generating a relation from an array of elements.
"""
function TetraFaceNeighbourRelation(elements::AbstractArray{Tetrahedron})

	adjacencyMat = faceAdjacencyFn(elements)

	rowN, colN = size(adjacencyMat)

	relation = Vector{SVector{4,Union{Missing,Int}}}(undef,colN)

	for ii in eachindex(relation)

		adjacencyCol = @view adjacencyMat[:,ii]

		relation[ii] = SVector{4}(adjacencyCol)

	end # for

	TetraFaceNeighbourRelation(relation)

end # function
