"""
	vector = drainFn(vector::AbstractVector)

Drains an input vector of all its elements without changing its capacity.
"""
function drainFn(vector::AbstractVector)

	while ! isempty(vector)

		pop!(vector)

	end # while

	vector

end # function
