"""
	Base.findlast(predicate::Function, element::AbstractConvexElement)

Finds the index of the last node index in element conforming to predicate.
"""
function Base.findlast(predicate::Function, element::AbstractConvexElement)

	Base.findlast(predicate,element.nodeI)

end # function
