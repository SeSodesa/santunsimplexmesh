"""
	edges = edgesFn(element::AbstractConvexElement)

Retrieves the set of edges of a complex element. Note that this must be
implemented on each concrete convex element type separately, as the topology of
an element cannot be defined generically.
"""
function edgesFn(element::AbstractConvexElement)

	error("Cannot sensibly fetch edges of an abstract convex element. Please define edgesFn for $(typeof(element)).")

end # function

"""
	edges = edgesFn(elements::AbstractVector)

Vectors of (vectors of) edges should get recursively flattened to a single vector of edges.
"""
function edgesFn(elements::AbstractArray{<:AbstractConvexElement})

	collect( Base.Iterators.flatmap( edgesFn, elements ) )

end # function

"""
	edge = edgesFn(element::Edge)

The edges of an Edge are the Edge itself.
"""
function edgesFn(element::Edge)

	element

end # function

"""
	edges = edgesFn(element::Voxel)

Retrieves the edges of a Voxel.
"""
function edgesFn(element::Voxel)

	SVector{12,Edge}(
		Edge(element.nodeI[SVector(1,2)]),
		Edge(element.nodeI[SVector(2,3)]),
		Edge(element.nodeI[SVector(3,4)]),
		Edge(element.nodeI[SVector(4,1)]),
		Edge(element.nodeI[SVector(1,5)]),
		Edge(element.nodeI[SVector(2,6)]),
		Edge(element.nodeI[SVector(3,7)]),
		Edge(element.nodeI[SVector(4,8)]),
		Edge(element.nodeI[SVector(5,6)]),
		Edge(element.nodeI[SVector(6,7)]),
		Edge(element.nodeI[SVector(7,8)]),
		Edge(element.nodeI[SVector(8,5)]),
	)

end # function

"""
	edges = edgesFn(element::Triangle)

Returns the edges of a given triangle.
"""
function edgesFn(element::Triangle)

	SVector{3,Edge}(
		Edge(element.nodeI[SVector(1,2)]),
		Edge(element.nodeI[SVector(1,3)]),
		Edge(element.nodeI[SVector(2,3)]),
	)

end # function

"""
	edges = edgesFn(element::Tetrahedron)

Returns the edges of a tetrahedron.
"""
function edgesFn(element::Tetrahedron)

	SVector{6,Edge}(
		Edge(element.nodeI[SVector(1,2)]),
		Edge(element.nodeI[SVector(1,3)]),
		Edge(element.nodeI[SVector(1,4)]),
		Edge(element.nodeI[SVector(2,3)]),
		Edge(element.nodeI[SVector(2,4)]),
		Edge(element.nodeI[SVector(3,4)]),
	)

end # function
