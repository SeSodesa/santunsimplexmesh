"""
	VoxelMesh <: AbstractConvexMesh

A mesh consisting of voxels.
"""
struct VoxelMesh <: AbstractConvexMesh
	__nodes :: Vector{Node{3}}
	__elements :: Vector{Voxel}
	__elementNeighbours :: Vector{SVector{6,Union{Missing,Int}}}
	xN :: Int
	yN :: Int
	zN :: Int
	xRes :: Float64
	yRes :: Float64
	zRes :: Float64
end

"""
	voxelMesh = VoxelMesh(
		xmin::Real,
		xmax::Real,
		xres::Real,
		ymin::Real,
		ymax::Real,
		yres::Real,
		zmin::Real,
		zmax::Real,
		zres::Real,
	)

An external constructor for VoxelMesh.
"""
function VoxelMesh(
	xmin::Real,
	xmax::Real,
	xres::Real,
	ymin::Real,
	ymax::Real,
	yres::Real,
	zmin::Real,
	zmax::Real,
	zres::Real,
)

	@assert xmin <= xmax "xmin > xmax"
	@assert ymin <= ymax "ymin > ymax"
	@assert zmin <= zmax "zmin > zmax"

	@assert xres > 0 "xres ≤ 0"
	@assert yres > 0 "yres ≤ 0"
	@assert zres > 0 "zres ≤ 0"

	@assert xres ≤ xmax - xmin "xres ≤ 0"
	@assert yres ≤ ymax - ymin "yres ≤ 0"
	@assert zres ≤ zmax - zmin "zres ≤ 0"

	# Generate iterators for different point dimensions.

	xrange = xmin : xres : xmax
	yrange = ymin : yres : ymax
	zrange = zmin : zres : zmax

	# Compute numbers of voxels in different dimensions.

	Nex = length(xrange) - 1
	Ney = length(yrange) - 1
	Nez = length(zrange) - 1

	Nnx = Nex + 1
	Nny = Ney + 1
	Nnz = Nez + 1

	# Generate nodes.

	nodes = [ Node{3}(x,y,z) for z in zrange for y in yrange for x in xrange ]

	# Preallocate voxels.

	elements = Vector{Voxel}(undef, Nex * Ney * Nez)

	Ne = length(elements)

	ni = 1

	for ei in 1 : Ne

		ci = ei - 1

		xi = ci % Nex + 1
		yi = ci ÷ Nex % Ney + 1
		zi = ci ÷ Nex ÷ Ney + 1

		# Node indexing nI with voxels goes as follows: with 8 voxels we'd have
		#
		#  nI ∈ {
		#    1, 2,
		#    4, 5,
		#    10, 11,
		#    13, 14
		#  }
		#
		# and with 64 there'd be
		#
		#  nI ∈ {
		#    1,2,3,4,
		#    6,7,8,9,
		#    11,12,13,14,
		#    16,17,18,19,
		#    26,27,28,29,
		#    ...
		#  }
		#
		# In other words, when  a line break is encoutnered, 2 nodes are
		# skipped and when a page break is encountered, skip 2 + number of
		# nodes in a row nodes.

		i1 = ni
		i2 = ni + 1
		i3 = ni + Nnx * Nny+1
		i4 = ni + Nnx * Nny
		i5 = ni + Nnx
		i6 = ni + Nnx + 1
		i7 = ni + Nnx + Nnx * Nny + 1
		i8 = ni + Nnx + Nnx * Nny

		elements[ei] = Voxel(SVector(i1,i2,i3,i4,i5,i6,i7,i8))

		# For now, manipulate the node index ni manually like this according to the above logic.
		# There might also be a more mathematical solution to this using xi--zi.

		linebreak = xi == Nex

		pagebreak = yi == Ney && linebreak

		ni += if pagebreak
			2 + Nnx
		elseif linebreak
			2
		else
			1
		end # if

	end # for

	# Finally, compute and cache initial adjacency information.

	adjacencyMat = faceAdjacencyFn(elements)

	VoxelMesh(
		nodes,
		elements,
		collect(SVector{6}(col) for col in eachcol(adjacencyMat)),
		Nex,
		Ney,
		Nez,
		xres,
		yres,
		zres
	)

end # function
