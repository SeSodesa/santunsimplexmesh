"""
	faceElementPairs = nonMissingNeighboursFn(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation})

Finds the neighbours of each element in a given relation.
"""
function nonMissingNeighboursFn(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation})

	elN = length(relation)

	out = [ sizehint!( Pair{Int8,Int}[], 4 ) for _ in eachindex(relation.relation) ]

	for (ii,elementNeighbours) in enumerate(relation)

		for (jj,elI) in enumerate(elementNeighbours)

			if ! ismissing(elI)

				push!( out[ii], jj => elI )

			end # if

		end # for

	end # for

	out

end # function
