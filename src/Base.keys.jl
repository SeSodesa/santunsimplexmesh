"""
	Base.keys(element::AbstractConvexElement)

Returns an iterator over the node indices in an element.
"""
function Base.keys(element::AbstractConvexElement)

	( element.nodeI[ii] for ii in eachindex(element.nodeI) )

end # function
