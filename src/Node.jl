"""
	Node{D}

A type alias for a static vector of dimension D.
"""
const Node{D} = SVector{D,Float64} where D

"""
	Node1 = Node{1}

A type alias for nodes of dimension 1.
"""
const Node1 = Node{1}

"""
	Node2 = Node{2}

A type alias for nodes of dimension 2.
"""
const Node2 = Node{2}

"""
	Node3 = Node{3}

A type alias for nodes of dimension 3.
"""
const Node3 = Node{3}
