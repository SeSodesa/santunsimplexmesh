"""
	edgeRefinement4(element::Tetrahedron, nodes::AbstractArray{Node3}, splitFaceI::Int, lastNodeI::Int)

Splits a tetrahedron into 4 tetrahedra, by splitting the 3 edges of a single
face and drawing edges between the new nodes and the vertex opposite to the
split face.
"""
function edgeRefinement4(element::Tetrahedron, nodes::AbstractArray{Node3}, splitFaceI::Int, lastNodeI::Int)

	elementFaces = element |> surfaceFn

	relevantFace = elementFaces[splitFaceI]

	fe1, fe2, fe3 = faceEdges = relevantFace |> edgesFn

	newNodes = centroidFn(faceEdges, nodes)

	ecN = length(newNodes)

	ni1, ni2, ni3 = newNodeI = SVector{ecN,Int}( lastNodeI .+ ( 1 : ecN ) )

	ei1, ei2, ei3, ei4 = element.nodeI

	newElements = if splitFaceI == 1 # ei123

		SVector(
			Tetrahedron(ei1,ni1,ni2,ei4),
			Tetrahedron(ei3,ni2,ni3,ei4),
			Tetrahedron(ei2,ni3,ni1,ei4),
			Tetrahedron(ni1,ni3,ni2,ei4),
		)

	elseif splitFaceI == 2 # ei134

		SVector(
			Tetrahedron(ei1,ni1,ni2,ei2),
			Tetrahedron(ei3,ni3,ni1,ei2),
			Tetrahedron(ei4,ni2,ni3,ei2),
			Tetrahedron(ni1,ni3,ni2,ei2),
		)

	elseif splitFaceI == 3 # ei142

		SVector(
			Tetrahedron(ei1,ni1,ni2,ei3),
			Tetrahedron(ei4,ni3,ni1,ei3),
			Tetrahedron(ei2,ni2,ni3,ei3),
			Tetrahedron(ni1,ni3,ni2,ei3),
		)

	elseif splitFaceI == 4 # ei243

		SVector(
			Tetrahedron(ei2,ni1,ni2,ei1),
			Tetrahedron(ei4,ni3,ni1,ei1),
			Tetrahedron(ei3,ni2,ni3,ei1),
			Tetrahedron(ni1,ni3,ni2,ei1),
		)

	else

		SVector(
			Tetrahedron(-1),
			Tetrahedron(-1),
			Tetrahedron(-1),
			Tetrahedron(-1),
		)

	end # if

	newElements, newNodes, newNodeI

end # function
