"""
	centroidFn(element::AbstractConvexElement,nodes::AbstractVector{Node{D}}) where D

Computes the centroid of a convex element, that is referencing a set of points.
"""
function centroidFn(element::AbstractConvexElement,nodes::AbstractVector{Node{D}}) where D

	vertices = nodes[element]

	sum( vertices ) / length( vertices )

end # function

"""
	centroidFn(elements::AbstractVector{<:AbstractConvexElement},nodes::AbstractVector{Node{D}}) where D

Computes the centroids of a set of convex elements, that are referencing a set of points.
"""
function centroidFn(elements::AbstractVector{<:AbstractConvexElement},nodes::AbstractVector{Node{D}}) where D

	elementN = elements |> length

	out = Vector{Node{D}}(undef,elementN)

	for (ii,element) in enumerate(elements)

		out[ii] = centroidFn( element, nodes )

	end # for

	out

end # function

"""
	centroidFn(elements::SVector{N,<:AbstractConvexElement},nodes::AbstractVector{Node{D}}) where { N, D }

Computes the centroids of a set of convex elements, that are referencing a set of points.
"""
function centroidFn(elements::SVector{N,<:AbstractConvexElement},nodes::AbstractVector{Node{D}}) where { N, D }

	out = MVector{N,Node{D}}(undef)

	for (ii,element) in enumerate(elements)

		out[ii] = centroidFn( element, nodes )

	end # for

	SVector(out)

end # function
