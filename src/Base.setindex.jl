"""
	Base.setindex(element::AbstractConvexElement, value::Integer, I::Integer)

Implements setting the node indices inside of an element with a single integer index.
Returns the modified element.
"""
function Base.setindex(element::AbstractConvexElement, value::Integer, I::Integer)

	newNodeI = Base.setindex(element.nodeI,value,I)

	typeof(element)(newNodeI)

end # function
