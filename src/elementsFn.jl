"""
	elements = elementsFn(m :: AbstractConvexMesh)

Returns the elements of a given convex mesh.
"""
function elementsFn(m :: AbstractConvexMesh)

	m.__elements

end # function
