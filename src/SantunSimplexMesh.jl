"""
	module SantunSimplexMesh

Santtu's module that contains types and functions for dealing with simplical meshes.
"""
module SantunSimplexMesh

export @disp
export AbstractConvexElement
export AbstractConvexMesh
export AbstractSimplexElement
export AbstractSimplexMesh
export Edge
export Node
export Node1
export Node2
export Node3
export Rectangle
export TetraFaceNeighbourRelation
export TetraMeshAlternating5
export Tetrahedron
export Triangle
export TriangleFaceNeighbourRelation
export Voxel
export VoxelMesh
export areaFn
export centroidFn
export centroidVolumeRefinement
export distanceToEdge
export distanceToTriangle
export drainFn
export edgeAdjacencyFn
export edgeRefinement2
export edgeRefinement4
export edgeVolumeRefinement
export edgesFn
export elementsFn
export elementsfn
export enclosedBy
export faceAdjacencyFn
export nodesFn
export nodesFn
export nonMissingNeighboursFn
export standardElementTransformation
export surfaceFn
export swapRemove!
export vertexFn
export volumeFn

using StaticArrays
using LinearAlgebra

# Type definitions. Note that the order in which these files are loaded
# matters, as you can't define a type in terms of others, before first defining
# the other types.

include("AbstractTypes.jl")
include("Edge.jl")
include("Node.jl")
include("Rectangle.jl")
include("Voxel.jl")
include("VoxelMesh.jl")
include("Tetrahedron.jl")
include("TetraFaceNeighbourRelation.jl")
include("Triangle.jl")
include("TriangleFaceNeighbourRelation.jl")
include("TetraMeshAlternating5.jl")
include("TriangleMesh.jl")

# Function definitions.

"""
	disp(ex)

Displays a given variable name with its value in human-readable form.
"""
macro disp(ex)
	quote
		print( $(string(ex)), " = " ); display( $(esc(ex)) )
	end
end

include("Base.==.jl")
include("Base.findall.jl")
include("Base.findfirst.jl")
include("Base.findlast.jl")
include("Base.getindex.jl")
include("Base.isless.jl")
include("Base.iterate.jl")
include("Base.keys.jl")
include("Base.length.jl")
include("Base.reverse.jl")
include("Base.setindex!.jl")
include("Base.setindex.jl")
include("Base.size.jl")
include("Base.sort.jl")
include("areaFn.jl")
include("centroidFn.jl")
include("centroidVolumeRefinement.jl")
include("distanceToEdge.jl")
include("distanceToTriangle.jl")
include("drainFn.jl")
include("edgeAdjacencyFn.jl")
include("edgeRefinement2.jl")
include("edgeRefinement4.jl")
include("edgeVolumeRefinement.jl")
include("edgesFn.jl")
include("elementsFn.jl")
include("enclosedBy.jl")
include("faceAdjacencyFn.jl")
include("nodesFn.jl")
include("nonMissingNeighboursFn.jl")
include("sortSurfElementAndLabelsFn.jl")
include("standardElementTransformation.jl")
include("surfElementsAndLabelsFn.jl")
include("surfaceFn.jl")
include("swapRemove.jl")
include("vertexFn.jl")
include("volumeFn.jl")

"""
	tetraVec = to5Tetra(voxel::Voxel)

Subdivides a voxel to 5 tetrahedra.
"""
function to5Tetra(voxel::Voxel)

	SVector{5,Tetrahedron}(
		Tetrahedron(voxel.nodeI[SVector(1,2,3,6)]),
		Tetrahedron(voxel.nodeI[SVector(1,3,4,8)]),
		Tetrahedron(voxel.nodeI[SVector(1,6,3,8)]),
		Tetrahedron(voxel.nodeI[SVector(1,6,8,5)]),
		Tetrahedron(voxel.nodeI[SVector(3,8,6,7)]),
	)

end # function

"""
	tetraVec = to5TetraMirrored(voxel::Voxel)

Subdivides a voxel to 5 tetrahedra, in a way that mirrors the function
to5Tetra.
"""
function to5TetraMirrored(voxel::Voxel)

	SVector{5,Tetrahedron}(
		Tetrahedron(voxel.nodeI[SVector(1,2,4,5)]),
		Tetrahedron(voxel.nodeI[SVector(2,3,4,7)]),
		Tetrahedron(voxel.nodeI[SVector(2,4,5,7)]),
		Tetrahedron(voxel.nodeI[SVector(2,5,6,7)]),
		Tetrahedron(voxel.nodeI[SVector(4,5,7,8)]),
	)

end # function

"""
	nodesHaveAllowedDims(nodes::Matrix{Float64},allowedNodeDims::Vector{Int})

Checks that the given nodes have an allowed dimensionality.
"""
function nodesHaveAllowedDims( nodes, allowedDims )

	nodeCoords = size(nodes,1)

	if nodeCoords ∈ allowedDims
		true
	else
		false
	end

end # function

"""
	elementsHaveNVertices(elements,allowedSize)

Checks that given elements have an allowed number of vertices.
"""
function elementsHaveNVertices( elements, N )

	nVertices = size(elements,1)

	if nVertices == N
		true
	else
		false
	end

end # function

"""
Checks that there are no node indices in elements that refer to non-existent
nodes, and that all nodes are covered by the elements.
"""
function allElementsReferToNodes( nodes, elements )

	nodeAmount = size(nodes,2)

	nodeIs = falses(1,nodeAmount)

	for nodeI ∈ elements

		if 1 ≤ nodeI ≤ nodeAmount

			nodeIs[nodeI] = true

		else

			return false

		end # if

	end # for

	all(nodeIs)

end # function

"""
	bool = solidAngleLabelingFn( point::Node{D}, surfP::AbstractArray{Node{D}}, surfT::AbstractArray{Triangle} ) where D

Determines whether a given point is enclosed by a surface consisting of
triangles and their respective vertex coordinates using the solid angle method.
Not that the orientation of the triangles needs to be such that the surface
normals as computed via the right-hand rule point out of the surface.
"""
function solidAngleLabelingFn( point::Node{D}, surfP::AbstractArray{Node{D}}, surfT::AbstractArray{Triangle}; threshold::Float64=0.25 ) where D

	# TODO: check that the norming of areas and differences done below is the one intended.

	@assert 0 ≤ threshold ≤ 1 "The given threshold T needs to hold up the relation 0 ≤ T ≤ 1."

	vertexCoords = surfP[surfT]

	triangleCentroids = centroidFn(surfT,surfP)

	triangleAreaVecs = areaFn(surfT,surfP)

	areaNorms = norm.(triangleAreaVecs)

	normedAreas = triangleAreaVecs ./ areaNorms

	posDiffs = triangleCentroids .- [point]

	squaredDistances = sum.( dot.( posDiffs, posDiffs ) )

	@disp squaredDistances

	posDiffNorms = norm.(posDiffs)

	normedPosDiffs = posDiffs ./ posDiffNorms

	dots = dot.(normedPosDiffs, triangleAreaVecs)

	@show solidAngleIntegral = sum( dots ) / 4 / π

	solidAngleIntegral ≥ threshold

end # function

end # module SantunSimplexMesh
