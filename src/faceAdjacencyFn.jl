"""
	adjacencyMat = faceAdjacencyFn(elements::AbstractArray{<:Union{Triangle,Tetrahedron}})

Computes the adjacency relation of a given set of simplical elements. The
output is a matrix of Union{Int,Missing}, where columns refer to elements and
rows refer to the face along which the element is connected to its neighbour.
"""
function faceAdjacencyFn(elements::AbstractArray{<:Union{Triangle,Tetrahedron,Voxel}})

	# Get vectors surface triangles of all elements.

	surfElementVec, elementLabels, faceLabels, eleN, faceN = surfElementsAndLabelsFn(elements)

	sortedSurfElementVec, sortedElementLabels, sortedFaceLabels, nonSurfMask = sortSurfElementAndLabels(surfElementVec, elementLabels, faceLabels, eleN, faceN)

	# Store the adjacency information into a dictionary that maps element to
	# pairs of neigbours and connecting surface elements.

	elementI = sortedElementLabels[nonSurfMask]

	faceI = sortedFaceLabels[nonSurfMask]

	adjacencyRelation = Matrix{Union{Int,Missing}}(missing,faceN,eleN)

	for ii in eachindex( elementI )

		# The neighbouring element is either the next or previous one.

		eI = elementI[ii]

		nI = if isodd(ii)
			elementI[ii + 1]
		else
			elementI[ii - 1]
		end

		fI = faceI[ii]

		adjacencyRelation[fI,eI] = nI

	end # for

	adjacencyRelation

end # function
