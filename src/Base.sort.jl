"""
	sortedElement = Base.sort(element::AbstractConvexElement; kwargs...)

Sorts the node indices inside of a convex element and returns an element of the
same type with the sorted indices.
"""
function Base.sort(element::AbstractConvexElement; kwargs...)

	typeof(element)( sort( element.nodeI; kwargs... ) )

end # function
