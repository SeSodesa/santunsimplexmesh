"""
	Rectangle <: AbstractConvexElement

A type representing a rectangle (a 4-tuple of node indices).
"""
struct Rectangle <: AbstractConvexElement
	nodeI :: SVector{4,Int}
end #  struct

"""
	Rectangle(a::Integer)

An external convenience constructor for Rectangles. Note that technically this
constructor does not make sense, as an element referencing the same point with
all of its vertices is degenerate, but it might come in handy when generating
dummy elements or such.
"""
function Rectangle(a::Integer)

	Rectangle((a,a,a,a))

end # function

"""
	Rectangle(a::Integer,b::Integer,c::Integer,d::Integer)

An external convenience constructor for Rectangles.
"""
function Rectangle(a::Integer,b::Integer,c::Integer, d::Integer)

	Rectangle((a,b,c,d))

end # function
