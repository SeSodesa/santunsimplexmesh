"""
	surface = surfaceFn(mesh::AbstractSimplexMesh)

Retrieves the surface of a a simplex mesh.
"""
function surfaceFn(mesh::AbstractSimplexMesh)

	mesh |> elementsFn |> surfaceFn

end # function

"""
	surface = surfaceFn(element::AbstractConvexElement)

Retrieves the surface of a complex element. Note that this must be implemented
on each concrete convex element type separately, as the topology of an element
cannot be defined generically.
"""
function surfaceFn(element::AbstractConvexElement)

	error("Cannot sensibly fetch the surface of an abstract convex element. Please define surfaceFn for $(typeof(element)).")

end # function

"""
	surface = surfaceFn(elements::AbstractVector{<:AbstractVector})

Vectors of vectors of element surfaces should get recursively flattened to a
single vector of surface elements.
"""
function surfaceFn(elements::AbstractArray{<:AbstractConvexElement})

	collect( Base.Iterators.flatmap( surfaceFn, elements ) )

end # function

"""
	surface = surfaceFn(element::Edge)

The surface of an edge is the set of 2 points that the edge consists of or the edge itself.
"""
function surfaceFn(element::Edge)

	element

end # function

"""
	surface = surfaceFn(element::Triangle)

Returns the "surface" of a triangle: its edges.
"""
function surfaceFn(element::Triangle)
	edgesFn(element)
end

"""
	surface = surfaceFn(element::Voxel)

Returns the "surface" of a voxel: the (outward-oriented) rectangles that make
up its faces.
"""
function surfaceFn(element::Voxel)

	SVector{6}(
		Rectangle(element.nodeI[SVector(1,4,3,2)]),
		Rectangle(element.nodeI[SVector(1,2,6,5)]),
		Rectangle(element.nodeI[SVector(2,3,7,6)]),
		Rectangle(element.nodeI[SVector(3,4,8,7)]),
		Rectangle(element.nodeI[SVector(4,1,5,8)]),
		Rectangle(element.nodeI[SVector(5,6,7,8)]),
	)

end # function

"""
	surface = surfaceFn(element::Tetrahedron)

Returns the surface triangles of a tetrahedron. Returns the surfaces in
the order -y, -x, -z and +xyz.
"""
function surfaceFn(element::Tetrahedron)
	SVector{4,Triangle}(
		Triangle(element.nodeI[SVector(1,2,3)]),
		Triangle(element.nodeI[SVector(1,3,4)]),
		Triangle(element.nodeI[SVector(1,4,2)]),
		Triangle(element.nodeI[SVector(2,4,3)]),
	)
end

"""
	surfaceTri, elementI, faceI = surfaceFn(elements::AbstractVector{Union{Triangle,Tetrahedron}})

Generates a set of surface elements for a set of triangles or tetrahedra.
"""
function surfaceFn(elements::AbstractArray{<:Union{Triangle,Tetrahedron}})

	surfElementVec, elementLabels, faceLabels, eleN, faceN = surfElementsAndLabelsFn(elements)

	sortedSurfElementVec, sortedElementLabels, sortedFaceLabels, nonSurfMask = sortSurfElementAndLabels(surfElementVec, elementLabels, faceLabels, eleN, faceN)

	surfMask = .! nonSurfMask

	sortedSurfElementVec[surfMask], sortedElementLabels[surfMask], sortedFaceLabels[surfMask]

end # function
