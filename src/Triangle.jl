"""
	Triangle <: AbstractSimplexElement

A triangle holding onto 3 node indices.
"""
struct Triangle <: AbstractSimplexElement
	nodeI :: SVector{3,Int}
end

"""
	Triangle(a::Integer)

An external convenience constructor for Triangles. Note that technically this
constructor does not make sense, as an element referencing the same point with
all of its vertices is degenerate, but it might come in handy when generating
dummy elements or such.
"""
function Triangle(a::Integer)

	Triangle((a,a,a))

end # function

"""
	Triangle(a::Integer,b::Integer,c::Integer)

An external convenience constructor for Triangles.
"""
function Triangle(a::Integer,b::Integer,c::Integer)

	Triangle((a,b,c))

end # function
