"""
	Tetrahedron <: AbstractSimplexElement

A tetrahedron holding onto 4 node indices.
"""
struct Tetrahedron <: AbstractSimplexElement
	nodeI :: SVector{4,Int}
end

"""
	Tetrahedron(a::Integer)

An external convenience constructor for Tetrahedra.
"""
function Tetrahedron(a::Integer)

	Tetrahedron((a,a,a,a))

end # function

"""
	Tetrahedron(a::Integer,b::Integer,c::Integer)

An external convenience constructor for Tetrahedra.
"""
function Tetrahedron(a::Integer,b::Integer,c::Integer,d::Integer)

	Tetrahedron((a,b,c,d))

end # function
