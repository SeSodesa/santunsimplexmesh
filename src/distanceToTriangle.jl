"""
	distance = distanceToTriangle(point::Node{N},element::Triangle,nodes::AbstractArray{Node{N}}) where N

Computes the distance of a given point to a given triangle.
"""
function distanceToTriangle(point::Node{N},element::Triangle,nodes::AbstractArray{Node{N}}) where N

	# First project the point onto the plane define by the triangle edges.

	planeNormal = areaFn(element, nodes)

	triangleArea = norm(planeNormal)

	unitNormal = planeNormal / triangleArea

	(A, B, C) = vertexFn(element,nodes)

	pointVec = point - A

	distanceToPlane = dot( pointVec, unitNormal )

	pp = pointProjection = pointVec - distanceToPlane * unitNormal

	# Find out the areal coordinates of point P in the plane.

	App = pp - A

	Bpp = pp - B

	Cpp = pp - C

	areaVecA = cross( C - B, Bpp ) / 2

	areaVecB = cross( A - C, Cpp ) / 2

	areaVecC = cross( B - A, App ) / 2

	areaA = norm( areaVecA )

	areaB = norm( areaVecB )

	areaC = norm( areaVecC )

	uaA = unitAreaA = areaVecA / areaA

	uaB = unitAreaB = areaVecB / areaB

	uaC = unitAreaC = areaVecC / areaC

	noA = negativeOriantationA = isapprox(uaA, - unitNormal)

	noB = negativeOriantationB = isapprox(uaB, - unitNormal)

	noC = negativeOriantationC = isapprox(uaC, - unitNormal)

	# Determine the distance based on the signed areal coordinates. The
	# logicals follow from the anticommutative nature of the cross product.

	insideTriangle = all( ( ! noA, ! noB, ! noC ) )

	outsideOppositeToA = noA && ! noB && ! noC

	outsideOppositeToB = ! noA && noB && ! noC

	outsideOppositeToC = ! noA && ! noB && noC

	nearAngleA = ! noA && noB && noC

	nearAngleB = noA && ! noB && noC

	nearAngleC = noA && noB && ! noC

	(AB,AC,BC) = edgesFn(element)

	if insideTriangle

		abs(distanceToPlane)

	elseif nearAngleA

		norm(point - A)

	elseif nearAngleB

		norm(point - B)

	elseif nearAngleC

		norm(point - C)

	elseif outsideOppositeToA

		distanceToEdge(point, BC, nodes)

	elseif outsideOppositeToB

		distanceToEdge(point, AC, nodes)

	elseif outsideOppositeToC

		distanceToEdge(point, AB, nodes)

	else

		error("None of the 7 possible distance criteria were satisfied for distanceToTriangle. How odd…")

	end # if

end # function
