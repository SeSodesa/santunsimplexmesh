"""
	Base.findall(predicate::Function, element::AbstractConvexElement)

Finds the indices of all node indices in element conforming to predicate.
"""
function Base.findall(predicate::Function, element::AbstractConvexElement)

	Base.findall(predicate,element.nodeI)

end # function
