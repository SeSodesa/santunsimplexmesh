"""
	TriangleMesh{D <: Integer}

A type that represents a triangular mesh with D dimensions (usually 2 or 3).
"""
struct TriangleMesh{D <: Integer} <: AbstractSimplexMesh

	"""
	The nodes in the mesh.
	"""
	__nodes :: Vector{Node{D}}

	"""
	The elements of this mesh.
	"""
	__elements :: Vector{Triangle}

end # TriangleMesh{D}
