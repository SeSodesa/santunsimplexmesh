"""
	Base.findfirst(predicate::Function, element::AbstractConvexElement)

Finds the index of the first node index in element conforming to predicate.
"""
function Base.findfirst(predicate::Function, element::AbstractConvexElement)

	Base.findfirst(predicate,element.nodeI)

end # function
