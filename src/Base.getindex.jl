
"""
	Base.getindex(array::AbstractArray, element::AbstractConvexElement)

Implements indexing into abstract arrays with convex elements.
"""
function Base.getindex(array::AbstractArray, element::AbstractConvexElement)

	array[element.nodeI]

end # function

"""
	Base.getindex(array::AbstractArray, elements::AbstractArray{<:AbstractConvexElement})

Implements indexing into abstract arrays with convex elements.
"""
function Base.getindex(array::AbstractArray, elements::AbstractArray{<:AbstractConvexElement})

	reduce( vcat, [ array[element.nodeI] for element in elements ] )

end # function

"""
	Base.getindex(array::AbstractArray, elements::AbstractArray{<:AbstractArray{<:AbstractConvexElement}})

Implements indexing into abstract arrays with convex elements.
"""
function Base.getindex(array::AbstractArray, elements::AbstractArray{<:AbstractArray{<:AbstractConvexElement}})

	reduce( vcat, [ array[element] for element in elements ] )

end # function

"""
	Base.getindex(element::AbstractConvexElement, I::Integer)

Indexes into a convex element with an integer.
"""
function Base.getindex(element::AbstractConvexElement, I::Integer)

	element.nodeI[I]

end # function

"""
	Base.getindex(element::AbstractConvexElement, I::AbstractArray)

Indexes into a convex element with any subtype of AbstractArray.
"""
function Base.getindex(element::AbstractConvexElement, I::AbstractArray)

	element.nodeI[I]

end # function

"""
	Base.getindex(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation},I::Integer)

Gets the neighbours of a single element of index I.
"""
function Base.getindex(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation},I::Integer)

	relation.relation[I]

end # function

"""
	Base.getindex(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation},I...)

Gets the neighbours of a elements specified by I...
"""
function Base.getindex(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation}, I...)

	[ relation.relation[ii] for ii in I ]

end # function
