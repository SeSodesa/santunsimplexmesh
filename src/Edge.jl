"""
	Edge <: AbstractSimplexElement

A type representing line segments between two separate Node{D}.
"""
struct Edge <: AbstractSimplexElement
	nodeI :: SVector{2,Int}
end # struct

"""
	Edge(a::Integer)

An external convenience constructor for Edge. Note that technically this
constructor does not make sense, as an element referencing the same point with
all of its vertices is degenerate, but it might come in handy when generating
dummy elements or such.
"""
function Edge(a::Integer)

	Edge((a,a))

end # function

"""
	Edge(a::Integer,b::Integer)

An external convenience constructor for Edge.
"""
function Edge(a::Integer,b::Integer)

	Edge((a,b))

end # function
