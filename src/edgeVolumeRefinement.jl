"""
	elements, nodes, surfRelation = edgeVolumeRefinement(element::Tetrahedron, nodes::AbstractArray{Node3}, lastNodeI::Integer)

Subdivides a tetrahedron into 8 new tetra by adding new nodes at commonEdge
centroids and drawing required new edges between these points. Also
returns a relation, whose columns map to the new elements and rows to
which face of the new elements are in touch with which face of the
original element.
"""
function edgeVolumeRefinement(element::Tetrahedron, nodes::AbstractArray{Node3}, lastNodeI::Integer)

	# Get relevant element information.

	elementEdges = edgesFn(element)

	edgeCoords = nodes[elementEdges]

	edgeCentroids = centroidFn(elementEdges,nodes)

	ecN = length(edgeCentroids)

	newNodeIs = SVector{ecN,Int}( lastNodeI .+ ( 1 : ecN ) )

	# Subdivide given element using the new node indices. See edgesFn to find
	# out the order in which it returns the undivided element edges. These
	# magic numbers will make more sense after that.

	ei1, ei2, ei3, ei4 = element.nodeI

	ni12, ni13, ni14, ni23, ni24, ni34 = newNodeIs

	elementsOut = SVector{8}(
		Tetrahedron((ei1, ni12, ni13, ni14)),
		Tetrahedron((ei2, ni23, ni12, ni24)),
		Tetrahedron((ei3, ni13, ni23, ni34)),
		Tetrahedron((ni12, ni23, ni13, ni14)),
		Tetrahedron((ni13, ni23, ni34, ni14)),
		Tetrahedron((ni12, ni23, ni14, ni24)),
		Tetrahedron((ni14, ni23, ni34, ni24)),
		Tetrahedron((ni14, ni24, ni34, ei4)),
	)

	elementsOut, edgeCentroids, newNodeIs

end # function

"""
	newElements, newNodes = edgeVolumeRefinement(elements::AbstractArray{Tetrahedron}, nodes::AbstractArray{Node3}, adjacencyRelation::AbstractMatrix{Vector{Int}}; elementN::Int=length(elements))

Subdivides a tetrahedron into 8 new tetra by adding new nodes at commonEdge centroids
and drawing required new edges between these points.
"""
function edgeVolumeRefinement(elements::AbstractArray{Tetrahedron}, nodes::AbstractArray{Node3}, adjacencyRelation::AbstractMatrix{Vector{Int}};elementN::Int=length(elements))

	# Start out by defining a few local helper functions.

	"""
	Goes over given elements and replaces each occurrence of a new redundant
	node with a corresponding "old" or already existing one.
	"""
	function removeRedundantNodesFromElements(newElements, newNodeI, oldNodeI, newElementsM)

		newElementsM[:] = newElements

		elL = newElements[begin] |> length

		nodeIM = MVector{elL,Int}(undef)

		for (ii, element) in enumerate(newElementsM)

			nodeIM[:] = element.nodeI

			for (jj,nodeI) in enumerate(element.nodeI)

				nodeI == newNodeI ? nodeIM[jj] = oldNodeI : nothing

			end # for

			newElementsM[ii] = eltype(elements)(nodeIM)

		end # for elI

		SVector(newElementsM)

	end # function

	"""
	Go over given new set of elements and replace non-redundant nodes in the
	elements with new shifted values.
	"""
	function updateNonRedundantNodesInElements( newElements, newNodeI, oldNodeI, newElementsM )

		newElementsM[:] = newElements

		elL = newElements[begin] |> length

		nodeIM = MVector{elL,Int}(undef)

		for (ii, element) in enumerate(newElementsM)

			nodeIM[:] = element.nodeI

			for (nI, oI) in zip(newNodeI,oldNodeI)

				for (kk,nodeI) in enumerate(element.nodeI)

					nodeI == oI ? nodeIM[kk] = nI : nothing

				end # for kk

			end # for (nI,oI)

			newElementsM[ii] = eltype(elements)(nodeIM)

		end # for ii

		SVector(newElementsM)

	end # function

	"""
	Finds out which elements can be found in both arrays. Generally one should
	use the intersect function, but this is here for the very specific purpose
	of reducing allocations down to 1 per call.
	"""
	function commonEdgesFn(edges1, edges2)

		I = SVector{6,Bool}(false,false,false,false,false,false)

		for (ii, edgei) in enumerate(edges1)

			for (jj,edgej) in enumerate(edges2)

				if edgei == edgej

					I = setindex(I,true,ii)

				end # if

			end # for jj

		end # for ii

		edges1[I]

	end # function

	elN = length(elements)

	splitN = 8

	edgeN = 6

	elementsOut = Vector{Tetrahedron}(undef, splitN * elN)

	# 6 edges per tetrahdron, so 6 new nodes per tetra.

	nodesOut = sizehint!(Node3[], edgeN * elN)

	ei = 1

	# Use this to keep track of which elements have already been refined. The
	# neighbours of an already refined element will introduce redundant nodes,
	# that need to be excluded.

	alreadyRefined = falses( elementN, 1 )

	# Preallocate a few arrays to reduce allocations in the hot loop.

	newNodeIMat = Matrix{Int}(undef, edgeN, elN)

	lastNodeI = length(nodes)

	redundantNodeB = edgeN |> falses

	nonRedundantNodeB = edgeN |> falses

	nonRedundantNodeI = Vector{Int}(undef,edgeN)

	newNonRedundantNodeI = Vector{Int}(undef,edgeN)

	newElementsM = MVector{splitN,eltype(elements)}(undef)

	# Go over elements, generate new elements and ditch redundant ones.

	for eI in eachindex(elements)

		element = elements[eI]

		alreadyRefined[eI] = true

		newElements, newNodes, newNodeIs = edgeVolumeRefinement(element, nodes, lastNodeI)

		newNodeIMat[:,eI] = newNodeIs

		# Find the neighbours of the element that was just subdivided, and
		# which edges they shared before refinement.

		neighbourI = unique( Base.Iterators.flatten( @view adjacencyRelation[:,eI] ) )

		elementEdges = edgesFn( element )

		for nI in neighbourI

			# If an neighbouring element was not already refined, we do not
			# need to run the replacement loop, as it will not have introduced
			# new nodes.

			! alreadyRefined[nI] ? continue : nothing

			# Check for redundant nodes in case the neighbour was already
			# subdivided. These will be at the midpoints of edges that the
			# current element and the neighbours share.

			neighbourElement = elements[nI]

			neighbourEdges = edgesFn(neighbourElement)

			commonEdges = commonEdgesFn(elementEdges, neighbourEdges)

			# Find the indices of the common edges in the newly subdivided
			# element and the neighbouring element. These also indicate
			# which nodes in the current element need to be replaced with
			# the nodes of the already subdivided neighbours.

			for commonEdge in commonEdges

				eeI = elementEdgeI = findfirst( e -> e == commonEdge, elementEdges )

				redundantNodeB[eeI] = true

				neI = neighbourEdgeI = findfirst( e -> e == commonEdge, neighbourEdges )

				# Take the newly generated elements and nodes, and replace
				# redundant node indices and nodes in them with existing
				# ones.

				newNodeI = newNodeIs[eeI]

				oldNodeI = newNodeIMat[neI,nI]

				newNodeIMat[eeI,eI] = oldNodeI

				# Find new elements that contain redundant nodeI and go
				# over them, modifying them to contain an older
				# corresponding one.

				newElements = removeRedundantNodesFromElements( newElements, newNodeI, oldNodeI, newElementsM)

			end # for commonEdge

		end # for nI

		# Now that we can determine which newly generated nodes were
		# non-redundant, shift them down, so the node index range remains
		# contiguous. Also update the last node position in tandem.

		for (ii,bool) in enumerate(redundantNodeB)

			nonRedundantNodeB[ii] = ! bool

		end # for

		# This abomination of a loop used to set up nonRedundantNodeI for this
		# round is just to reduce allocations.

		nonRedundantNodeI = nonRedundantNodeI |> drainFn

		for (ii,(bool,ind)) in enumerate( zip( nonRedundantNodeB, @view newNodeIMat[:, eI] ) )

			if bool

				push!(nonRedundantNodeI, ind)

			end # if

		end # for

		# Do a similar thing to update newNonRedundantiNodeI without allocations.

		newNonRedundantNodeI = newNonRedundantNodeI |> drainFn

		for ii in eachindex(nonRedundantNodeI)

			push!( newNonRedundantNodeI, lastNodeI + ii )

		end # for ii

		newNodeIMat[nonRedundantNodeB, eI] = newNonRedundantNodeI

		lastNodeI += length(nonRedundantNodeI)

		# Go over the new elements again and also modify the non-redundant nodes

		newElements = updateNonRedundantNodesInElements( newElements, newNonRedundantNodeI, nonRedundantNodeI, newElementsM)

		# Save non-redundant new nodes to the set of output nodes.

		erange = ei : ei + splitN - 1

		elementsOut[erange] = newElements

		for (ii, (node,bool)) in enumerate(zip(newNodes,nonRedundantNodeB))

			bool ? push!(nodesOut, newNodes[ii]) : nothing

		end # for ii

		# Update indexing variables and pointers.

		ei += splitN

		redundantNodeB[:] .= false

	end # for eI

	elementsOut, nodesOut, newNodeIMat

end # function

"""
	newElements, newNodes = edgeVolumeRefinement(elements::AbstractArray{Tetrahedron}, nodes::AbstractArray{Node3}, edgeAdjacencyRelation::Matrix{Vector{Int}}, elementsI::AbstractArray{Integer}, surfEleIs::AbstractArray{<:Integer})

Refines a subset of elements based on elementsI. Also needs to refine the
immediate neighbours of the elements being refined to maintain mesh
consistency or water-tightness.
"""
function edgeVolumeRefinement(elements::AbstractArray{Tetrahedron}, nodes::AbstractArray{Node3}, edgeAdjacencyRelation::Matrix{Vector{Int}}, faceAdjacencyRelation::Matrix{Union{Int,Missing}}, elementsI::AbstractArray{<:Integer}, surfEleIs::AbstractArray{<:Integer})

	# Get subset of elements to refine.

	elementsView = @view elements[elementsI]

	adjacencyView = @view edgeAdjacencyRelation[:,elementsI]

	newElements, newNodes, newNodeIMat = edgeVolumeRefinement(elementsView, nodes, adjacencyView;elementN=length(elements))

	newElementParentI = repeat( elementsI, inner=8 )

	nodeN = length(nodes) + length(newNodes)

	# Once the desired part of the volume has been refined, also refine
	# the immediate neighbours outside of the subvolume.

	surfView = @view elements[surfEleIs]

	surfNeighbourView = @view edgeAdjacencyRelation[:,surfEleIs]

	unrefinedI = setdiff( eachindex(elements), elementsI )

	# Preallocate some scratch space arrays for below loop.

	uniqueEdgeNeighbourI = sizehint!(Int[],5)

	uniqueNeighbourEdgeCounts = sizehint!(Int[],5)

	edgeIndices = sizehint!(Int[],5)

	# Go over surface elements within the refined subvolume, find out which of
	# their edge neighbours are outside of the subvolume, and how many times
	# the edge neigbours occur in the adjacency relation. The number of
	# occurences affects how they should be refined.

	for (ii,surfEleI) in enumerate(surfEleIs)

		surfElement = elements[surfEleI]

		subElementsOfSurfElem = @view newElements[newElementParentI .== surfEleI]

		nodesAddedToSurfElem = @view newNodeIMat[:,surfEleI]

		surfNeighbourIter = @view surfNeighbourView[:,ii]

		for (edgei, elemivec) in enumerate(surfNeighbourIter)

			for elemi in elemivec

				if elemi in unrefinedI

					if ! (elemi in uniqueEdgeNeighbourI)

						push!(uniqueEdgeNeighbourI,elemi)

						push!(uniqueNeighbourEdgeCounts, 1)

						push!(edgeIndices, edgei)

					else

						I = findfirst( ==(elemi), uniqueEdgeNeighbourI )

						uniqueNeighbourEdgeCounts[I] += 1

					end # if

				end # if

			end # for elemi

		end # for edgei, elemvec

		# Now that we have collected the neighbours and how many edges they
		# share with the current element and the connected edge, generate new
		# elements based on the information.

		for (jj,(ueni,ec)) in enumerate(zip(uniqueEdgeNeighbourI,uniqueNeighbourEdgeCounts))

			neighbourElement = elements[ueni]

			newNeighElements, newNeighNodes, newNeighNodeI = if ec == 3

				faceNeighbours = @view faceAdjacencyRelation[:,ueni]

				splitFaceI = findfirst( x -> ! ismissing(x) && x == surfEleI, faceNeighbours )

				edgeRefinement4(neighbourElement,nodes, splitFaceI, nodeN)

			else # ec == 1

				edgeI = edgeIndices[jj]

				edgeRefinement2(neighbourElement, nodes, edgeI, nodeN)

			end # if

			# After generating the new elements, remember to replace all new
			# node indices in them with old ones, as the new elements reuse the
			# new nodes generated when refining the subvolume.

			newNeighElementsM = MVector(newNeighElements)

			for neigbourElement in newNeighElements

				@disp neigbourElement

				elNodeIM = MVector(neigbourElement.nodeI)

				for nodeI in newNeighNodeI

					nieI = findfirst( ==(nodeI), neigbourElement)

					isnothing(nieI) ? continue : nothing

					# TODO: replace node index at I with an older node index.
					# Need to determine them somehow from the elements generated
					# during the subvolume refinement. Maybe also return which
					# new element was generated from which tetrahedron when
					# calling edgeVolumeRefinement above?

					# Get which node indices were added to the surface element
					# in the subvolume.

					@disp surfEleI

					@disp surfElement

					@disp neighbourElement

					@disp edgeI = edgeIndices[jj]

					@disp nodesAddedToSurfElem

					@disp subElementsOfSurfElem

				end # for nodeI

			end # for neigbourElement

		end # for jj, ueni

		# Reset preallocated arrays.

		uniqueEdgeNeighbourI = uniqueEdgeNeighbourI |> drainFn

		uniqueNeighbourEdgeCounts = uniqueNeighbourEdgeCounts |> drainFn

		edgeIndices = edgeIndices |> drainFn

	end # for ii

	newElements, newNodes

end # function
