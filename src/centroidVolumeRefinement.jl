"""
	elements, node = centroidVolumeRefinement(element::Tetrahedron, nodes::AbstractArray{Node3})

Subdivides a tetrahedron into 4 tetrahedra volumetrically, by inserting a new
point into the volumetric centroid of the tetrahedral vertices and connecting
each vertex to the new point via a line segment.
"""
function centroidVolumeRefinement(element::Tetrahedron, nodes::AbstractArray{Node3})

	# Get centroid and relevant parts of element.

	elementCentroid = centroidFn( element, nodes )

	elementFaces = surfaceFn( element )

	lastNodeI = length( nodes )

	newNodeI = lastNodeI + 1

	# Generate set of new elements.

	elements = SVector{4,Tetrahedron}(
		Tetrahedron((elementFaces[1]...,newNodeI,)),
		Tetrahedron((elementFaces[2]...,newNodeI,)),
		Tetrahedron((elementFaces[3]...,newNodeI,)),
		Tetrahedron((elementFaces[4]...,newNodeI,)),
	)

	elements, elementCentroid

end # function

"""
	elements, nodes = centroidVolumeRefinement(elements::AbstractArray{Tetrahedron}, nodes::AbstractArray{Node3})

Subdivides a set of given tetrahedra into 4 tetrahedra volumetrically, by
inserting a new point into the volumetric centroid of the tetrahedral vertices
and connecting each vertex to the new point via a line segment.
"""
function centroidVolumeRefinement(elements::AbstractArray{Tetrahedron}, nodes::AbstractArray{Node3})

	elN = length(elements)

	splitN = elements[end] |> length

	elementsOut = Vector{Tetrahedron}(undef, splitN * elN)

	nodesOut = Vector{Node3}(undef, elN)

	jj = 1

	for ii in eachindex(elements)

		els, node = centroidVolumeRefinement(elements[ii], nodes)

		range = jj : jj + splitN - 1

		elementsOut[range] .= els

		nodesOut[ii] = node

		jj += splitN

	end # for

	elementsOut, nodesOut

end # function
