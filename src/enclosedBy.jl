"""
	TF = enclosedBy(point::Node{D}, surfTriangles::AbstractArray{Triangle}, surfPoints::AbstractArray{Node{D}}) where D

Determines whether a point is located inside of a given triangular surface.
"""
function enclosedBy(point::Node{D}, surfTriangles::AbstractArray{Triangle}, surfPoints::AbstractArray{Node{D}}) where D

	# Find closest triangle.

	distances = [ distanceToTriangle(point,triangle,surfPoints) for triangle in surfTriangles ]

	minI = argmin( distances )

	# Form a tetrahedron out of the triangle and the point.

	surfTriV = surfPoints[surfTriangles[minI]]

	vertices = [ surfTriV..., point ]

	# Compute the signed volume of the tetrahedron. The location in relation to
	# the surface is determined by the sign of this volume.

	volume = volumeFn(Tetrahedron,vertices)

	inside = sign(volume) > 0

end # function
