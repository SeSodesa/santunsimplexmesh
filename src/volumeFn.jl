"""
	error = volumeFn(element::AbstractConvexElement,nodes::AbstractArray{Node3})

It is an error to try and compute a volume of a non-concrete element.
"""
function volumeFn(element::AbstractConvexElement,nodes::AbstractArray{Node{D}}) where {D}

	error("Abstract convex elements do not have a volume. Please implement volumeFn for $(typeof(element)).")

end # function

"""
	volume = volumeFn(element::Tetrahedron,nodes::AbstractArray{Node3})

Computes the signed volume of a tetrahedron, based on the nodes it references.
"""
function volumeFn(element::Tetrahedron,nodes::AbstractArray{Node3})

	× = LinearAlgebra.cross

	⋅ = LinearAlgebra.dot

	# First retrieve edges.

	V = vertices = vertexFn(element,nodes)

	edge1 = V[2] - V[1]
	edge2 = V[3] - V[1]
	edge3 = V[4] - V[1]

	# Note the order of the edges here: our tetra are oriented as if the
	# surface normals were pointing outward.

	edge2 × edge1 ⋅ edge3 / 6

end # function

"""
	volumes = volumeFn(elements::AbstractArray{<:AbstractConvexElement}, nodes::AbstractArray{Node{3}})

Generates an array of volumes for convex elements.
"""
function volumeFn(elements::AbstractArray{<:AbstractConvexElement}, nodes::AbstractArray{Node{D}}) where D

	[ volumeFn(element, nodes) for element in elements ]

end # function

"""
	volume = volumeFn(T::Type{Tetrahedron}, vertices::AbstractArray{Node3})

Computes the volume of a tetrahedron given as only a set of vertices.
"""
function volumeFn(T::Type{Tetrahedron}, vertices::AbstractArray{Node3})

	× = LinearAlgebra.cross

	⋅ = LinearAlgebra.dot

	edge1 = vertices[2] - vertices[1]
	edge2 = vertices[3] - vertices[1]
	edge3 = vertices[4] - vertices[1]

	# Note the order of the edges here: our tetra are oriented as if the
	# surface normals were pointing outward.

	edge2 × edge1 ⋅ edge3 / 6

end # function
