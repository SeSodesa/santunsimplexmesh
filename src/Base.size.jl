"""
	Base.size(element::AbstractConvexElement)

Implements size for convex elements as the number of vertices in the element.
"""
function Base.size(element::AbstractConvexElement)

	Base.size(element.nodeI)

end # function
