
"""
	sortedSurfElementVec, sortedElementLabels, sortedFaceLabels, nonSurfMask = sortSurfElementAndLabels(surfElementVec, elementLabels, faceLabels, eleN, faceN)

A helper for sorting surface element vectors.
"""
function sortSurfElementAndLabels(surfElementVec, elementLabels, faceLabels, eleN, faceN)

	nonSurfMask = falses( eleN * faceN )

	sortedSurfElemVecs = [ sort(element.nodeI) for element in surfElementVec ]

	surfElementPerm = sortperm(sortedSurfElemVecs)

	sortedSurfElementVec = surfElementVec[surfElementPerm]

	sortedNodeIndVecs = sortedSurfElemVecs[surfElementPerm]

	sortedElementLabels = elementLabels[surfElementPerm]

	sortedFaceLabels = faceLabels[surfElementPerm]

	# Subtract neighbouring rows from each other to check which triangles are
	# shared by different elements.

	differences = sortedNodeIndVecs[2:end] - sortedNodeIndVecs[1:end-1]

	nonSurfaceMask = all.( x -> x == 0, differences )

	nonSurfI = findall( nonSurfaceMask )

	nonSurfMask[nonSurfI] .= true

	nonSurfMask[nonSurfI.+1] .= true

	sortedSurfElementVec, sortedElementLabels, sortedFaceLabels, nonSurfMask

end # function
