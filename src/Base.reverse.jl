"""
	Base.reverse(element::AbstractConvexElement)

Reverses the node indices in a convex element.
"""
function Base.reverse(element::AbstractConvexElement)

	typeof(element)(Base.reverse(element.nodeI))

end # function
