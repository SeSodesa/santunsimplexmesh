"""
	nodes = nodesFn(m :: AbstractConvexMesh)

Returns the nodes of a given convex mesh.
"""
function nodesFn(m :: AbstractConvexMesh)

	m.__nodes

end # function
