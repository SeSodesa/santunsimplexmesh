"""
	edgeRefinement2(element::Tetrahedron,nodes::AbstractArray{Node3}, splitEdgeI::Int, lastNodeI::Int)

Splits a tetrahedron into 2 tetra, by adding a single node in the middle of a
given edge and drawing two new edges across the 2 faces attached to the split
edge.
"""
function edgeRefinement2(element::Tetrahedron,nodes::AbstractArray{Node3}, splitEdgeI::Int, lastNodeI::Int)

	elementEdges = element |> edgesFn

	relevantEdge = elementEdges[splitEdgeI]

	newNode = centroidFn(relevantEdge, nodes)

	ei1, ei2, ei3, ei4 = element.nodeI

	nnI = newNodeI = lastNodeI + 1

	# Generate new elements based on the order in which edgesFn returns the
	# edges: 12, 13, 14, 23, 24, 34.

	newElements = if splitEdgeI == 1

		SVector(
			Tetrahedron(ei1, nnI, ei3, ei4),
			Tetrahedron(nnI, ei2, ei3, ei4),
		)

	elseif splitEdgeI == 2

		SVector(
			Tetrahedron(ei1,ei2,nnI,ei4),
			Tetrahedron(nnI,ei2,ei3,ei4),
		)

	elseif splitEdgeI == 3

		SVector(
			Tetrahedron(ei1, ei2, ei3, nnI),
			Tetrahedron(nnI, ei2, ei3, ei4),
		)

	elseif splitEdgeI == 4

		SVector(
			Tetrahedron(ei1,ei2,nnI,ei4),
			Tetrahedron(ei1,nnI,ei3,ei4),
		)

	elseif splitEdgeI == 5

		SVector(
			Tetrahedron(ei1,ei2,ei3,nnI),
			Tetrahedron(ei1,nnI,ei3,ei4),
		)

	elseif splitEdgeI == 6

		SVector(
			Tetrahedron(ei1,ei2,ei3,nnI),
			Tetrahedron(ei1,ei2,nnI,ei4),
		)

	else

		SVector(
			Tetrahedron(-1),
			Tetrahedron(-1),
		)

	end # if

	newElements, Scalar(newNode), newNodeI

end # function
