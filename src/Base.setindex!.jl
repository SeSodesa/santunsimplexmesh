"""
	Base.setindex!(array::AbstractArray, values::Any, element::AbstractConvexElement)

Implements indexing into abstract arrays with convex elements.
"""
function Base.setindex!(array::AbstractArray, values::Any, element::AbstractConvexElement)

	array[element.nodeI] .= values

end # function
