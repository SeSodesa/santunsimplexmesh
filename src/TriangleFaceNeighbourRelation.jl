"""
	TriangleFaceNeighbourRelation <: AbstractNeighbourRelation

Represents an face-based adjacency relation between tetrahedra.
"""
struct TriangleFaceNeighbourRelation <: AbstractNeighbourRelation

	relation :: Vector{SVector{3,Union{Missing,Int}}}

end # struct

"""
	TriangleFaceNeighbourRelation(elements::AbstractArray{Triangle})

An external constructor for generating a relation from an array of elements.
"""
function TriangleFaceNeighbourRelation(elements::AbstractArray{Triangle})

	adjacencyMat = faceAdjacencyFn(elements)

	rowN, colN = size(adjacencyMat)

	relation = Vector{SVector{3,Union{Missing,Int}}}(undef,colN)

	for ii in eachindex(relation)

		adjacencyCol = @view adjacencyMat[:,ii]

		relation[ii] = SVector{3}(adjacencyCol)

	end # for

	TriangleFaceNeighbourRelation(relation)

end # function
