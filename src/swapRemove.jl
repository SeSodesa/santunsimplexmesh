"""
	modifiedArray = swapRemove!(array::AbstractVector, index::Integer)

Given a contiguous array and an index, swaps the element at the index with the
last element of the array, and then removes the element from the end of the
array. This avoids having to shift all elements towards the beginning of the
array.
"""
function swapRemove!(array::AbstractVector, index::Integer)

	array[index], array[end] = array[end], array[index]

	pop!(array)

	array

end # function

"""
	modifiedArray = swapRemove!(array::AbstractVector, indices::AbstractArray{<:Integer})

Runs swapRemove on a given array for each index in indices.
"""
function swapRemove!(array::AbstractVector, indices::AbstractArray{<:Integer})

	for ii in indices

		array = swapRemove!(array, ii)

	end # for

	array

end # function
