"""
	Base.length(element::AbstractConvexElement)

Implements length for convex elements as the number of vertices in the element.
"""
function Base.length(element::AbstractConvexElement)

	Base.length(element.nodeI)

end # function

"""
	Base.length(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation})

Returns the number of elemenst in a given relation.
"""
function Base.length(relation::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation})

	Base.length(relation.relation)

end # function
