"""
	Voxel <: AbstractConvexElement

A voxel holding onto 8 node indices.
"""
struct Voxel <: AbstractConvexElement
	nodeI :: SVector{8,Int}
end
