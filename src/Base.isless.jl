"""
	Base.isless(a::T, b::T) where { T <: AbstractConvexElement }

Compares two convex elements of the same type.
"""
function Base.isless(a::T, b::T) where { T <: AbstractConvexElement }

	a.nodeI < b.nodeI

end # function
