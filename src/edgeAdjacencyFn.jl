"""
	A = edgeAdjacencyFn(elements::AbstractArray{<:Union{Tetrahedron}})

Computes an edge-based adjacency relation for agiven set of elements. Elements
are in this relation, if they share an edge.
"""
function edgeAdjacencyFn(elements::AbstractArray{<:Union{Tetrahedron}};sizeHint::Int=5)

	outT = Matrix{Vector{Int}}

	if isempty(elements) return outT(undef,0,0) end

	edgeVecVec = edgesFn.(elements)

	edgeN = length(edgeVecVec[end])

	elemN = length(elements)

	# Preallocate output.

	A = outT(undef,edgeN, elemN)

	for ii in eachindex(A)
		A[ii] = sizehint!( [], sizeHint )
	end

	# Label arrays for keeping track of which edge belongs to which element

	elementLabels = repeat( 1:elemN, inner=edgeN )

	edgeLabels = repeat( 1:edgeN, outer=elemN )

	# Sort arrays, so that edges are next to each other.

	edgeVec = sort.( Base.Iterators.flatten(edgeVecVec) )

	sortP = sortperm( edgeVec )

	sortedEdgeVec = @view edgeVec[sortP]

	sortedElementLabels = @view elementLabels[sortP]

	sortedEdgeLabels = @view edgeLabels[sortP]

	# Start going over sorted edges and mark which elements are edge-adjacent into the output relation.

	rangeStart, rangeEnd = 1, 1

	edgesN = length(sortedEdgeVec)

	while rangeStart < edgesN

		edge = sortedEdgeVec[rangeStart]

		# Update range end for going over neighbouring elements.

		endEdge = edge

		while endEdge == edge && rangeEnd ≤ edgesN

			rangeEnd += 1

			endEdge = sortedEdgeVec[rangeEnd]

		end # while

		rangeEnd -= 1

		# Then go over edges in range and add adjacency information.

		edgeRange = rangeStart : rangeEnd

		for ii in edgeRange

			for jj in edgeRange

				if ii == jj continue end

				celI = currentElementI = sortedElementLabels[ii]

				nelI = neighbourElementI = sortedElementLabels[jj]

				cedI = currentEdgeI = sortedEdgeLabels[ii]

				nedI = neighbourEdgeI = sortedEdgeLabels[jj]

				push!( A[nedI,nelI], celI)

			end # for

		end # for

		rangeStart = rangeEnd + 1

		rangeEnd = rangeStart

	end # for edgeI

	A

end # function
