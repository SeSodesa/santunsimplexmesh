"""
	AbstractConvexMesh

An abstract supertype for meshes that consist of convex shapes.
"""
abstract type AbstractConvexMesh end

"""
	AbstractSimplexMesh <: AbstractConvexMesh

An abstract type that represents a mesh that contains simplical elements.
Functions that are common to all simplical meshes are implemented for this
type.
"""
abstract type AbstractSimplexMesh <: AbstractConvexMesh end

"""
	AbstractConvexElement

An abstract supertype for convex shapes.
"""
abstract type AbstractConvexElement end

"""
	AbstractSimplexElement <: AbstractConvexElement

An abstract supertype for simplical elements.
"""
abstract type AbstractSimplexElement <: AbstractConvexElement end

"""
	AbstractNeighbourRelation

A supertype for data structures representing connections between elements.
"""
abstract type AbstractNeighbourRelation end
