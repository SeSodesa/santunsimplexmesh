"""
	distance = distanceToEdge(node::Node{N}, edge::Edge, nodes::AbstractVector{Node{N}}) where N

Computes the distance of a given node to a given edge, based on which nodes the
edge references in the set of given nodes.
"""
function distanceToEdge(node::Node{N}, edge::Edge, nodes::AbstractVector{Node{N}}) where N

	elementVertices = vertexFn(edge,nodes)

	directionVec = elementVertices[end] - elementVertices[begin]

	pointVec = node - elementVertices[begin]

	unitDirectionVec = directionVec ./ norm( directionVec )

	dotProd = dot( pointVec, unitDirectionVec )

	distance = norm( pointVec - dotProd * unitDirectionVec )

end # function

"""
	distances = distanceToEdge(points::AbstractArray{Node{N}}, edge::Edge, nodes::AbstractVector{Node{N}}) where N

Computes the distances of given points to a given edge, based on which nodes
the edge references in the set of given nodes.
"""
function distanceToEdge(points::AbstractArray{Node{N}}, edge::Edge, nodes::AbstractVector{Node{N}}) where N

	distances = [ distanceToEdge(point,edge,nodes) for point in points ]

end # function
