"""
	areaVec = areaFn(element::Union{Triangle, Rectangle}, nodes::AbstractVector{Node{D}}) where D

Computes the area of a triangle, that references a set of nodes.
"""
function areaFn(element::Union{Triangle, Rectangle}, nodes::AbstractVector{Node{D}}) where D

	vertexCoords = nodes[element]

	areaFn(typeof(element), vertexCoords)

end # function

"""
	areaVecs = areaFn(elements::AbstractVector{<:Union{Triangle,Rectangle}},nodes::AbstractVector{Node{D}})

Computes signed areas for a set of given triangles, referencing a set of nodes.
"""
function areaFn(elements::AbstractVector{<:Union{Triangle,Rectangle}},nodes::AbstractVector{Node{D}}) where D

	[ areaFn(element,nodes) for element in elements ]

end # function

"""
	areaFn(T::Type{Triangle}, points::AbstractArray{Node{D}}) where D

Computes the area of a triangle given only as a set of vertices.
"""
function areaFn(T::Type{Triangle}, vertices::AbstractArray{Node{D}}) where D

	planeVec1 = vertices[2] - vertices[1]

	planeVec2 = vertices[3] - vertices[1]

	LinearAlgebra.cross(planeVec1, planeVec2) / 2

end # function

"""
	area = areaFn(T::Type{Rectangle}, vertices::AbstractArray{Node{D}}) where D

Computes the area of a rectangle given only as a set of vertices.
"""
function areaFn(T::Type{Rectangle}, vertices::AbstractArray{Node{D}}) where D

	planeVec1 = vertices[2] - vertices[1]

	planeVec2 = vertices[4] - vertices[1]

	LinearAlgebra.cross(planeVec1, planeVec2)

end # function

"""
	areas = areaFn(element::AbstractConvexElement, vertices::AbstractArray{Node{D}}) where D

Computes the surface areas for a given convex element, assuming that it
consists of a set of surface elements.
"""
function areaFn(element::AbstractConvexElement, vertices::AbstractArray{Node{D}}) where D

	areaFn(surfaceFn(element),vertices)

end # function
