"""
	Base.iterate(a::AbstractConvexElement)

Implements iteration for convex elements. This method is called first to
produce the initial state of the object for actual iteration.
"""
function Base.iterate(a::AbstractConvexElement)

	state = a.nodeI

	iterate( a, state )

end # function

"""
	Base.iterate(a::AbstractConvexElement, state)

Implements iteration for convex elements. This method does the actual iteration
by updating the state.
"""
function Base.iterate(a::AbstractConvexElement, state)

	if isempty(state) return nothing end

	first = state[begin]

	rest = state[2:end]

	first, rest

end # function

"""
	Base.iterate(a::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation})

Implements iteration for convex elements. This method is called first to
produce the initial state of the object for actual iteration.
"""
function Base.iterate(a::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation})

	state = a.relation

	iterate( a, state )

end # function

"""
	Base.iterate(a::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation}, state)

Implements iteration for convex elements. This method does the actual iteration
by updating the state.
"""
function Base.iterate(a::Union{TriangleFaceNeighbourRelation,TetraFaceNeighbourRelation}, state)

	if isempty(state) return nothing end

	first = state[begin]

	rest = @view state[2:end]

	first, rest

end # function
