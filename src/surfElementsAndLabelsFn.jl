"""
	elements, elementsLabels, faceLabels, elemN, faceN = surfElementsAndLabelsFn(elements::AbstractArray{<:AbstractSimplexElement})

A helper function for producing a flattened set of surface elements, with
labels indicating which surface element belongs to which input element and
which "face" the surface element is in each input element.
"""
function surfElementsAndLabelsFn(elements::AbstractArray{<:AbstractConvexElement})

	# Get vectors surface triangles of all tetrahedra.

	surfElementVecs = elements .|> surfaceFn

	elemN = length(elements)

	faceN = length(surfElementVecs[end])

	# Generate elementLabels and faceLabels to keep track of which triangle
	# belongs to which tetrahedron and which face in the tetrahedron each face
	# is (by sorting these whenever the triangle vector is sorted).

	elementLabels = repeat( 1:elemN, inner=faceN )

	faceLabels = repeat( 1:faceN, outer=elemN )

	# Flatten this to a single sorted vector of triangles (or their node Indices).
	# Also keep track of which elements and faces the triangles represent.

	surfElementVec = collect( Base.Iterators.flatten( elVec for elVec in surfElementVecs ) )

	surfElementVec, elementLabels, faceLabels, elemN, faceN

end # function
