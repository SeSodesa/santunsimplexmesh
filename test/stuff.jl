using SantunSimplexMesh

vm = VoxelMesh(
	0, 1, 1,
	0, 1, 1,
	0, 1, 1,
)

tm = vm |> TetraMeshAlternating5

@disp vm.__elementNeighbours

@disp tm.__elementNeighbours
